{
  description = "An Automated Testing System";
  inputs.flake-utils.url = github:numtide/flake-utils;
  inputs.bats-assert.url = github:ztombol/bats-assert;
  inputs.bats-assert.flake = false;
  inputs.bats-support.url = github:ztombol/bats-support;
  inputs.bats-support.flake = false;

  outputs = { self, nixpkgs, bats-assert, bats-support, flake-utils }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        bats = pkgs.runCommand "bats" { nativeBuildInputs = [ pkgs.makeWrapper ]; } ''
          mkdir -p $out/bin
          makeWrapper ${pkgs.bats}/bin/bats $out/bin/bats --prefix PATH : ${bats-assert}/src:${bats-support}/src
        '';
      in
      {
        defaultPackage = bats;
      }
    );
}
